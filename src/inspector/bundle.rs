use amethyst::{
    core::bundle::SystemBundle,
    ecs::prelude::{DispatcherBuilder, World},
    error::Error,
    assets::AssetStorage,
};

use crate::system::{ResizeCameraSystemDesc, FollowMouseSystemDesc};
use amethyst::prelude::WorldExt;
use amethyst::core::SystemDesc;
use crate::asset::GridProps;
use crate::component::{Board, Ship, PlacedShip, PlayerSelf, PlayerOpponent, FollowMouse, SnapToGrid, OnClick, BoundingBox};
use amethyst_inspector::{inspector, InspectorHierarchy};
use amethyst::core::{Named,Transform};
use amethyst::renderer::SpriteRender;
use crate::prefab::board::BoardPrefab;
use amethyst::assets::PrefabLoaderSystemDesc;
use std::fmt::Debug;
use amethyst::core::Parent;

pub struct InspectorBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for InspectorBundle {
    fn build(self, _world: &mut World, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<(), Error> {
        builder.add(Inspector, "inspector_system", &[]);
        builder.add(InspectorHierarchy::default(), "inspector_hierarchy_system", &[]);
        Ok(())
    }
}

inspector![
    Named, Transform, SpriteRender,
	Board,
	PlayerSelf, PlayerOpponent,
	Ship, PlacedShip,
	FollowMouse,
	SnapToGrid,
	OnClick, BoundingBox,
];