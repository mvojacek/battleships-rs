use amethyst::{
    core::bundle::SystemBundle,
    ecs::prelude::{DispatcherBuilder, World},
    error::Error,
    assets::AssetStorage,
};

use crate::system::{
    ResizeCameraSystemDesc,
    FollowMouseSystemDesc,
    SnapToGridSystemDesc,
    OnClickSystemDesc
};
use amethyst::prelude::WorldExt;
use amethyst::core::SystemDesc;
use crate::asset::GridProps;
use crate::component::{Board, Ship, PlacedShip, PlayerSelf, PlayerOpponent, FollowMouse};
use amethyst_inspector::{inspector, InspectorHierarchy};
use amethyst::core::{Named,Transform};
use amethyst::renderer::SpriteRender;
use crate::prefab::board::BoardPrefab;
use amethyst::assets::PrefabLoaderSystemDesc;
use std::fmt::Debug;
use amethyst::core::Parent;
use amethyst::input::BindingTypes;
use serde::export::PhantomData;

#[derive(Default)]
pub struct BaseBundle<T: BindingTypes> {
    _marker: PhantomData<T>,
}

impl<'a, 'b, T: BindingTypes> SystemBundle<'a, 'b> for BaseBundle<T> {
    fn build(self, world: &mut World, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<(), Error> {
        builder.add(ResizeCameraSystemDesc.build(world), "resize_camera_system", &[]);
        builder.add(FollowMouseSystemDesc.build(world), "follow_mouse_system", &[]);
        builder.add(SnapToGridSystemDesc.build(world), "snap_to_grid_system", &["follow_mouse_system"]);
        builder.add(OnClickSystemDesc::<T>::default().build(world), "on_click_system", &[]);

        world.insert(AssetStorage::<GridProps>::default());

        builder.add(PrefabLoaderSystemDesc::<BoardPrefab>::default().build(world), "prefab_loader_system", &[]);

        world.register::<Board>();
        world.register::<Ship>();
        world.register::<PlacedShip>();
        world.register::<PlayerSelf>();
        world.register::<PlayerOpponent>();
        Ok(())
    }
}
