#![allow(unused_imports)]
#![recursion_limit = "10000"]

#[macro_use]
extern crate derivative;

use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    utils::application_root_dir,
    LoggerConfig, Logger,
    ui::UiBundle,
    input::StringBindings,
};
use log::LevelFilter;
use amethyst_imgui::RenderImgui;
use amethyst::input::InputBundle;
use crate::prefab::board::BoardPrefab;
use crate::inspector::InspectorBundle;

mod state;
mod component;
mod asset;
mod bundle;
mod system;
#[macro_use]
mod util;
mod prefab;
mod inspector;

fn main() -> amethyst::Result<()> {
    //prefab::board::board_test();
    //panic!();

    Logger::from_config(LoggerConfig::default())
        .level_for("gfx_backend_vulkan", LevelFilter::Off)
        .start();

    let app_root = application_root_dir()?;

    let resources = app_root.join("resources");
    let display_config = resources.join("display_config.ron");

    let game_data = {
        let mut game_data = GameDataBuilder::default()
            .with_bundle(TransformBundle::new())?
            .with_bundle(InputBundle::<StringBindings>::default())?
            .with_bundle(
                RenderingBundle::<DefaultBackend>::new()
                    .with_plugin(
                        RenderToWindow::from_config_path(display_config)
                            .with_clear([0.34, 0.36, 0.52, 1.0]),
                    )
                    .with_plugin(RenderFlat2D::default())
                    .with_plugin(RenderImgui::<StringBindings>::default()),
            )?
            .with_bundle(UiBundle::<StringBindings>::new())?
            .with_bundle(bundle::BaseBundle::<StringBindings>::default())?;
        if cfg!(feature = "inspector") {
            game_data = game_data.with_bundle(InspectorBundle)?;
        }
        game_data
    };

    let mut game = Application::new(resources, state::InitialState::default(), game_data)?;
    game.run();

    Ok(())
}
