use amethyst::core::math::base::{Vector2};
use amethyst::assets::{Asset,Format};
use serde_derive::{Serialize,Deserialize};
use amethyst::Error;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst::assets::Handle;
use ron::de::from_bytes as from_ron_bytes;

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct GridProps {
    dims: Vector2<u32>,
    cell_size: Vector2<u32>,
    centers_x: Vec<u32>,
    centers_y: Vec<u32>,
}

impl Asset for GridProps {
    const NAME: &'static str = "michal::GridProps";
    type Data = Self;
    type HandleStorage = DenseVecStorage<Handle<Self>>;
}

#[derive(Clone, Debug)]
pub struct GridPropsFormat;

impl Format<GridProps> for GridPropsFormat {
    fn name(&self) -> &'static str {
        "GRID_PROPS"
    }

    fn import_simple(&self, bytes: Vec<u8>) -> Result<GridProps, Error> {
        let grid_props: GridProps =
            from_ron_bytes(&bytes).map_err(|_| Error::from_string("loading grid_props"))?;

        //TODO assert grid centers length corresponds to grid size

        Ok(grid_props)
    }
}
