use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    audio::{output::Output, Source},
    assets::AssetStorage,
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
};
use amethyst::core::math::base::Vector4;

use log::info;
use crate::component::SnapToGrid;
use itertools::Itertools;
use std::cmp::Ordering;
use std::ops::Sub;

#[derive(SystemDesc, Default)]
#[system_desc(name(SnapToGridSystemDesc))]
pub struct SnapToGridSystem;

impl<'a> System<'a> for SnapToGridSystem {
    type SystemData = (
        ReadStorage<'a, SnapToGrid>,
        WriteStorage<'a, Transform>,
    );

    fn run(&mut self, (snaps, mut transforms): Self::SystemData) {
        for (snap, follower) in (&snaps, &mut transforms).join() {
            let t = follower.translation_mut();
            if let Some(&x) = snap.centers_x.iter().find(|&x| x.sub(t.x).abs() < snap.radius.x) {
                if let Some(&y) = snap.centers_y.iter().find(|y| y.sub(t.y).abs() < snap.radius.y) {
                    t.x = x;
                    t.y = y;
                }
            }
        }
    }
}