use amethyst::{
    input::{InputHandler, BindingTypes},
    winit::WindowEvent,
    winit::Event as WEvent,
    winit::{MouseButton, ElementState, ModifiersState},
    prelude::*,
    assets::AssetStorage,
    audio::{output::Output, Source},
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, Write, ReadExpect, ReadStorage, System, SystemData, WriteStorage, Entities, Entity},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
    window::ScreenDimensions,
};
use crate::component::{OnClick, BoundingBox};
use log::info;
use once_cell::unsync::Lazy;
use std::ops::Deref;
use serde::export::PhantomData;
use itertools::Itertools;
use amethyst::prelude::SystemDesc;
use crate::util::IntoVector2;
use crate::unwrap_or_return;

#[derive(SystemDesc)]
#[system_desc(name(OnClickSystemDesc))]
pub struct OnClickSystem<T: BindingTypes> {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<WEvent>,
    _marker: PhantomData<T>,
}

impl<T: BindingTypes> OnClickSystem<T> {
    fn new(reader_id: ReaderId<WEvent>) -> Self {
        Self { reader_id, _marker: PhantomData }
    }
}

impl<'a, T: BindingTypes> System<'a> for OnClickSystem<T> {
    type SystemData = (
        Read<'a, EventChannel<WEvent>>,
        ReadStorage<'a, OnClick>,
        ReadStorage<'a, BoundingBox>,
        ReadStorage<'a, Transform>,
        ReadExpect<'a, InputHandler<T>>,
        Entities<'a>,
        Write<'a, EventChannel<OnClickEvent>>,
        ReadExpect<'a, ScreenDimensions>,
        ReadStorage<'a, Camera>,
    );

    fn run(&mut self, (events, onclicks, bbs, transforms, input, entities, mut chan, screen, cameras): Self::SystemData) {
        let (camera, transform) = unwrap_or_return!((&cameras, &transforms).join().next());
        let (mouse_x, mouse_y) = unwrap_or_return!(input.mouse_position());
        //let [mouse_x, mouse_y]: [_;2] = crate::util::screen_to_world_global(mouse_x, mouse_y, camera, transform, &*screen).into_vec2().as_ref(); //FIXME uncommenting this line causes rustc to overflow

        for event in events.read(&mut self.reader_id) {
            if let WEvent::WindowEvent {
                window_id: _, event: WindowEvent::MouseInput {
                    device_id: _, state, button, modifiers
                }
            } = event {
                let (&button, &state, &modifiers) = (button, state, modifiers);
                if chan.would_write() {
                    let events = (&onclicks, &bbs, &transforms, &*entities).join()
                        .filter_map(|(onclick, bb, transform, entity)| {
                            if onclick.filter.should_fire(button, modifiers, state) {
                                let matrix = transform.global_matrix();
                                // info!("global {:?}", &matrix);
                                let global_bb = bb.transform(&matrix);
                                // info!("bb {:?}", &bb);
                                // info!("global bb {:?}", &global_bb);
                                if global_bb.contains(mouse_x, mouse_y) {
                                    return Some(entity);
                                }
                            }
                            return None;
                        })
                        .map(|entity| OnClickEvent {
                            entity,
                            button,
                            state,
                            modifiers,
                        }).collect_vec();
                    chan.iter_write(events);
                }
            }
        }
    }

    fn setup(&mut self, world: &mut World) {
        let chan = EventChannel::<OnClickEvent>::default();
        world.insert(chan)
    }
}

#[derive(Debug, Clone)]
pub struct OnClickEvent {
    pub entity: Entity,
    pub button: MouseButton,
    pub state: ElementState,
    pub modifiers: ModifiersState,
}