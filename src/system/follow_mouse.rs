use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    assets::AssetStorage,
    audio::{output::Output, Source},
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
    window::ScreenDimensions,
    core::math::{Vector3,Point3},
};

use log::info;
use crate::component::FollowMouse;
use once_cell::unsync::Lazy;
use std::ops::Deref;
use crate::util::{ToVector4, IntoVector3, global_to_transform_local, screen_to_world_global};

#[derive(SystemDesc)]
#[system_desc(name(FollowMouseSystemDesc))]
pub struct FollowMouseSystem {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<WEvent>,
}

impl FollowMouseSystem {
    fn new(reader_id: ReaderId<WEvent>) -> Self {
        Self { reader_id }
    }
}

impl<'a> System<'a> for FollowMouseSystem {
    type SystemData = (
        Read<'a, EventChannel<WEvent>>,
        ReadStorage<'a, FollowMouse>,
        WriteStorage<'a, Transform>,
        ReadExpect<'a, ScreenDimensions>,
        ReadStorage<'a, Camera>,
    );

    fn run(&mut self, (events, followers, mut transforms, screen, cameras): Self::SystemData) {
        for event in events.read(&mut self.reader_id) {
            if let WEvent::WindowEvent {
                window_id: _, event: WindowEvent::CursorMoved {
                    device_id: _, modifiers: _,
                    position: pos,
                }
            } = event {
                //info!("pos {} {}", pos.x, pos.y);
                let (camera, transform) = (&cameras, &transforms).join().next().unwrap();
                let world_point = screen_to_world_global(pos.x as f32, pos.y as f32, camera, transform, &*screen).to_vec4();
                //info!("world {:?}", &world_point);
                for (_, transform) in (&followers, &mut transforms).join() {
                    let new_local = global_to_transform_local(&world_point, &transform).into_vec3();
                    //info!("new_local {:?}", &new_local);
                    transform.set_translation(new_local);
                    //transform.set_translation_xyz(pos.x as f32, pos.y as f32, 0.);
                }
            }
        }
    }
}