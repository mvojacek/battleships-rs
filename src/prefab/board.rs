use amethyst::ecs::Component;
use crate::asset::GridProps;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize, Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use crate::component::{PlayerSelf, PlayerOpponent, Board};
use amethyst::renderer::sprite::prefab::SpriteScenePrefab;
use amethyst::assets::ProgressCounter;
use amethyst::core::Named;
use std::borrow::Cow;
use ron::ser::PrettyConfig;

#[derive(Debug, Clone, Serialize, Deserialize, PrefabData)]
pub struct BoardPrefab {
    name: Named,
    sprite_scene: SpriteScenePrefab,
    player: PlayerType,
    board: Board,
}

#[derive(Debug, Clone, Serialize, Deserialize, PrefabData)]
pub enum PlayerType {
    PlayerSelf(PlayerSelf),
    PlayerOpponent(PlayerOpponent),
}
