use amethyst::ecs::prelude::*;
use amethyst_inspector::Inspect;
use amethyst::ecs::Component;

pub mod board;
pub mod ship;
pub mod player;
pub mod snap_to_grid;
pub mod on_click;

pub use board::Board;
pub use ship::{Ship, PlacedShip};
pub use player::{PlayerSelf, PlayerOpponent};
pub use snap_to_grid::SnapToGrid;
pub use on_click::{OnClick, BoundingBox};

#[derive(Debug, Clone, Copy, Component, Default, Inspect)]
#[storage(NullStorage)]
pub struct FollowMouse;