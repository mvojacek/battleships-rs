use amethyst::ecs::Component;
use crate::asset::GridProps;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize, Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use crate::component::ship::ShipType;
use crate::component::{Board, Ship};
use amethyst::core::Transform;
use amethyst::core::math::base::Vector3;

#[derive(Clone, Component, Debug, Inspect)]
#[storage(DenseVecStorage)]
#[inspect(no_default)]
pub struct SnapToGrid {
    pub radius: Vector2<f32>,
    pub centers_x: Vec<f32>,
    pub centers_y: Vec<f32>,
}

impl SnapToGrid {
    pub fn ship_to_board(board_trans: &Transform, board: &Board, ship: &Ship) -> SnapToGrid {
        fn make_snaps(centers: &[f32], ship_cells: usize) -> Vec<f32> {
            let board_cells = centers.len();
            let snaps = board_cells - ship_cells as usize + 1;
            centers.iter().take(snaps).zip(centers.iter().skip(ship_cells as usize - 1))
                .map(|(a, b)| (a + b) / 2.).collect()
        }
        let &[ship_cells_x, ship_cells_y]: &[usize; 2] = ship.size_cells().as_ref();

        let mut snaps_x = make_snaps(&board.centers_x, ship_cells_x);
        let mut snaps_y = make_snaps(&board.centers_y, ship_cells_y);

        let translation = board_trans.translation();
        let scale: &Vector3<f32> = board_trans.scale();

        snaps_x.iter_mut().for_each(|x| *x = *x * scale.x + translation.x);
        snaps_y.iter_mut().for_each(|y| *y = *y * scale.y + translation.y);

        SnapToGrid {
            radius: board.cell_size.map(|i| i as f32 / 2.),
            centers_x: snaps_x,
            centers_y: snaps_y,
        }
    }
}