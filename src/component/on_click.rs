use amethyst::ecs::Component;
use crate::asset::GridProps;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize, Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use crate::component::ship::ShipType;
use crate::component::{Board, Ship};
use amethyst::core::Transform;
use amethyst::core::math::base::Vector3;
use amethyst::core::math::base::Matrix4;

#[derive(Component, Inspect, Derivative, Default)]
#[derivative(Debug)]
#[storage(DenseVecStorage)]
pub struct OnClick {
    pub filter: OnClickFilter,
}

use amethyst::winit::{MouseButton, ModifiersState, ElementState};

#[derive(Clone, Debug, Derivative)]
#[derivative(Default)]
pub struct OnClickFilter {
    pub button: Option<MouseButton>,
    pub modifiers: Option<ModifiersState>,
    #[derivative(Default(value="Some(ElementState::Released)"))]
    pub state: Option<ElementState>,
}

impl OnClickFilter {
    pub fn button(mut self, b: MouseButton) -> Self {
        self.button = Some(b);
        self
    }
    pub fn modifiers(mut self, m: ModifiersState) -> Self {
        self.modifiers = Some(m);
        self
    }
    pub fn state(mut self, s: ElementState) -> Self {
        self.state = Some(s);
        self
    }
}

impl OnClickFilter {
    pub fn should_fire(&self, b: MouseButton, m: ModifiersState, s: ElementState) -> bool {
        if let Some(button) = self.button {
            if button != b {
                return false;
            }
        }
        if let Some(state) = self.state {
            if state != s {
                return false;
            }
        }
        if let Some(mods) = self.modifiers {
            if (mods.ctrl && !m.ctrl)
                || (mods.alt && !m.alt)
                || (mods.shift && !m.shift)
                || (mods.logo && !m.logo) {
                return false;
            }
        }

        return true;
    }
}

use amethyst_inspector::{debug_inspect_control, InspectControl};
use std::ops::{DerefMut, Deref};

debug_inspect_control!(OnClickFilter);

#[derive(Clone, Component, Inspect, Debug, PrefabData)]
#[prefab(Component)]
#[storage(DenseVecStorage)]
#[inspect(no_default)]
pub struct BoundingBox {
    pub min: Vector2<f32>,
    pub max: Vector2<f32>,
}

impl BoundingBox {
    pub fn normalized(a: Vector2<f32>, b: Vector2<f32>) -> Self {
        let ([ax, ay], [bx, by]): ([_; 2], [_; 2]) = (a.into(), b.into());
        let (min_x, max_x) = if ax > bx { (bx, ax) } else { (ax, bx) };
        let (min_y, max_y) = if ay > by { (by, ay) } else { (ay, by) };
        BoundingBox {
            min: Vector2::new(min_x, min_y),
            max: Vector2::new(max_x, max_y),
        }
    }

    pub fn transform(&self, mat: &Matrix4<f32>) -> Self {
        use crate::util::{ToVector4, IntoVector2};

        Self::normalized((mat * self.min.to_vec4()).into_vec2(),
                         (mat * self.max.to_vec4()).into_vec2())
    }

    pub fn contains(&self, x: f32, y: f32) -> bool {
        x > self.min.x && x < self.max.x && y > self.min.y && y < self.max.y
    }
}