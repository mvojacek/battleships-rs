use amethyst::ecs::storage::NullStorage;
use amethyst::ecs::Component;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::Error;
use serde_derive::{Serialize, Deserialize};
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use amethyst::ecs::Entity;
use amethyst::assets::ProgressCounter;
use amethyst::assets::Handle;
use amethyst::core::math::base::Vector2;
use amethyst::ecs::prelude::*;
use derive_new::new;

#[derive(Debug, Clone, Copy, Default, Inspect, Component, PrefabData, Serialize, Deserialize)]
#[prefab(Component)]
#[storage(NullStorage)]
pub struct PlayerSelf;

#[derive(Debug, Clone, Copy, Default, Inspect, Component, PrefabData, Serialize, Deserialize)]
#[prefab(Component)]
#[storage(NullStorage)]
pub struct PlayerOpponent;

pub trait PlayerTag {}

impl PlayerTag for PlayerSelf {}

impl PlayerTag for PlayerOpponent {}
