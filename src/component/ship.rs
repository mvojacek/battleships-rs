pub use typ::{ShipType, ShipTypeEnum};
use crate::component::Board;
use amethyst::ecs::{Entity, Component};
use amethyst::prelude::*;
use amethyst::ecs::storage::{VecStorage, DenseVecStorage};
use amethyst::core::math::base::Vector2;
use crate::util::Direction2;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::ecs::prelude::ReadStorage;
use amethyst::ecs::join::Join;
use amethyst::Error;
use serde_derive::{Serialize, Deserialize};
use amethyst::core::Axis2;

pub mod typ {
    use std::marker::PhantomData;
    use serde_derive::{Serialize, Deserialize};

    use ShipTypeEnum::*;

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
    pub enum ShipTypeEnum {
        Carrier = 0,
        BattleShip,
        Cruiser,
        Submarine,
        PatrolBoat,
    }

    #[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
    pub struct ShipType {
        pub kind: ShipTypeEnum,
        pub length: usize,
        pub width: usize,
        pub texture_id: usize,
        #[serde(skip)]
        _marker: PhantomData<()>,
    }

    impl ShipType {
        const fn new(kind: ShipTypeEnum, length: usize, width: usize) -> Self {
            Self { kind, length, width, texture_id: kind as usize, _marker: PhantomData }
        }
    }

    pub const CARRIER: ShipType = ShipType::new(Carrier, 5, 1);
    pub const BATTLE_SHIP: ShipType = ShipType::new(BattleShip, 4, 1);
    pub const CRUISER: ShipType = ShipType::new(Cruiser, 3, 1);
    pub const SUBMARINE: ShipType = ShipType::new(Submarine, 3, 1);
    pub const PATROL_BOAT: ShipType = ShipType::new(PatrolBoat, 2, 1);

    pub const SHIPS_BY_TEX: [&'static ShipType; 5] = [
        &CARRIER, &BATTLE_SHIP, &CRUISER, &SUBMARINE, &PATROL_BOAT
    ];

    pub(crate) mod shiptype_serde {
        use serde::{Serializer, Deserializer, Serialize, Deserialize};
        use super::ShipType;
        use super::ShipTypeEnum;
        use super::SHIPS_BY_TEX;

        type Value = &'static ShipType;

        pub fn serialize<S>(val: &Value, ser: S) -> Result<S::Ok, S::Error>
            where S: Serializer {
            val.kind.serialize(ser)
        }

        pub fn deserialize<'de, D>(des: D) -> Result<Value, D::Error>
            where D: Deserializer<'de> {
            let kind = ShipTypeEnum::deserialize(des)?;
            Ok(SHIPS_BY_TEX[kind as usize])
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Derivative, Serialize, Deserialize)]
#[derivative(Default)]
pub enum ShipState {
    #[derivative(Default)]
    LIVE,
    SUNK,
}

#[derive(Debug, Clone, Inspect, Component, Serialize, Deserialize)]
#[storage(DenseVecStorage)]
#[inspect(no_default)]
pub struct Ship {
    #[inspect(skip)]
    #[serde(with = "typ::shiptype_serde")]
    pub typ: &'static ShipType,
    pub facing: Direction2,
}

impl Ship {
    pub fn size_cells(&self) -> Vector2<usize> {
        match self.facing.axis() {
            Axis2::X => Vector2::new(self.typ.length, self.typ.width),
            Axis2::Y => Vector2::new(self.typ.width, self.typ.length),
        }
    }
}

#[derive(Debug, Clone, Inspect, Component)]
#[storage(DenseVecStorage)]
#[inspect(no_default)]
pub struct PlacedShip {
    #[inspect(with_component = "crate::component::Board")]
    pub board: Entity,
    pub pos: Vector2<u32>,
    pub tiles: Vec<ShipState>,
    pub state: ShipState,
}

use amethyst_inspector::{debug_inspect_control, InspectControl};

debug_inspect_control![
    ShipState
];