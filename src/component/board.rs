use amethyst::ecs::Component;
use crate::asset::GridProps;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize,Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;

#[derive(Clone, Component, Debug, Deserialize, Serialize, PrefabData, Inspect)]
#[prefab(Component)]
#[storage(DenseVecStorage)]
#[inspect(no_default)]
pub struct Board {
    pub cell_size: Vector2<u32>,
    pub centers_x: Vec<f32>,
    pub centers_y: Vec<f32>,
}

#[allow(dead_code)]
impl Board {
    pub fn rows(&self) -> usize {
        self.centers_y.len()
    }

    pub fn cols(&self) -> usize {
        self.centers_x.len()
    }
}
