use amethyst::{
    assets::{AssetStorage, Loader},
    core::transform::Transform,
    input::{get_key, is_close_requested, is_key_down, VirtualKeyCode},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    ecs::prelude::{Read, ReadStorage, WriteStorage, Entities},
    window::ScreenDimensions,
    shrev::ReaderId, shrev::EventChannel,
    winit::{MouseButton, ModifiersState, ElementState},
};
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::ecs::Component;
use amethyst::ecs::join::Join;
use amethyst::core::math::base::Vector2;

use log::info;
use crate::asset::{GridProps, GridPropsFormat};
use crate::component::{Board, PlayerOpponent, PlayerSelf, Ship, FollowMouse, PlacedShip, SnapToGrid, BoundingBox, OnClick};
use amethyst::assets::Handle;
use crate::util::Direction2;
use crate::component::player::PlayerTag;
use std::borrow::Cow;
use amethyst::assets::PrefabLoader;
use crate::prefab::board::BoardPrefab;
use amethyst::assets::ProgressCounter;
use amethyst::assets::RonFormat;
use crate::system::OnClickEvent;
use once_cell::unsync::Lazy;
use crate::component::on_click::OnClickFilter;

#[derive(Default)]
pub struct InGameState {
    on_click: Option<ReaderId<OnClickEvent>>
}

impl SimpleState for InGameState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        self.on_click = Some(world.write_resource::<EventChannel<OnClickEvent>>().register_reader());

        init_ships(world);
    }

    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
        event: StateEvent,
    ) -> SimpleTrans {
        let world = data.world;

        if let StateEvent::Window(event) = &event {
            // Check if the window should be closed
            if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
                return Trans::Quit;
            }

            // Listen to any key events
            if let Some(event) = get_key(&event) {
                info!("handling key event: {:?}", event);
            }
        }

        self.handle_onclick(world);

        // Keep going
        Trans::None
    }
}

impl InGameState {
    fn handle_onclick(&mut self, w: &mut World) {
        w.exec(|(chan, ships, placed_ships, boards, selves, transforms, mut snaps, mut followers):
                (Read<EventChannel<OnClickEvent>>, ReadStorage<Ship>, ReadStorage<PlacedShip>,
                 ReadStorage<Board>, ReadStorage<PlayerSelf>, ReadStorage<Transform>,
                 WriteStorage<SnapToGrid>, WriteStorage<FollowMouse>)| {
            let board: Lazy<(&Board, &Transform), _> = Lazy::new(|| {
                let (board, transform, _) = (&boards, &transforms, &selves).join().next().unwrap();
                (board, transform)
            });
            for &OnClickEvent { entity, button, state: _, modifiers: _ }
            in chan.read(self.on_click.as_mut().unwrap()) {
                if button == MouseButton::Left {
                    if let Some(ship) = ships.get(entity) {
                        if placed_ships.get(entity).is_none() && followers.get(entity).is_none() {
                            // unplaced ship
                            followers.insert(entity, FollowMouse).unwrap();
                            let snap = SnapToGrid::ship_to_board(board.1, board.0, ship);
                            snaps.insert(entity, snap).unwrap();
                        }
                    }
                }
            }
        });
    }
}

fn init_ships(world: &mut World) {
    let texture = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "sprites/ships.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let sheet = {
        let loader = world.read_resource::<Loader>();
        let sheet_storage = world.read_resource::<AssetStorage<SpriteSheet>>();
        loader.load(
            "sprites/ships.ron",
            SpriteSheetFormat(texture),
            (),
            &sheet_storage,
        )
    };

    let transform = {
        let mut t = Transform::default();
        t.set_translation_xyz(100., 100., 0.);
        t
    };

    let patrol_boat_ship = crate::component::ship::Ship {
        typ: &crate::component::ship::typ::PATROL_BOAT,
        facing: Direction2::XPos,
    };
    let patrol_boat_sprite = SpriteRender {
        sprite_sheet: sheet,
        sprite_number: patrol_boat_ship.typ.texture_id,
    };

    let bb = BoundingBox {
        min: Vector2::new(-10., -10.),
        max: Vector2::new(10., 10.),
    };
    let onclick = OnClick {
        filter: OnClickFilter::default().button(MouseButton::Left),
    };

    world.create_entity()
        .named("patrol_boat")
        .with(transform)
        .with(patrol_boat_sprite)
        .with(patrol_boat_ship)
        .with(bb)
        .with(onclick)
        .build();
}
