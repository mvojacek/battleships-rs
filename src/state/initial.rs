use amethyst::{
    assets::{AssetStorage, Loader},
    core::transform::Transform,
    input::{get_key, is_close_requested, is_key_down, VirtualKeyCode},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    window::ScreenDimensions,
};
use log::info;
use crate::asset::GridProps;
use crate::component::Board;
use amethyst::assets::PrefabLoader;
use crate::prefab::board::BoardPrefab;
use amethyst::assets::ProgressCounter;
use amethyst::assets::RonFormat;

pub struct InitialState {
    progress: ProgressCounter,
}

impl Default for InitialState {
    fn default() -> Self {
        InitialState {
            progress: ProgressCounter::new(),
        }
    }
}

impl SimpleState for InitialState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        // Get the screen dimensions so we can initialize the camera and
        // place our sprites correctly later. We'll clone this since we'll
        // pass the world mutably to the following functions.
        let dimensions = (*world.read_resource::<ScreenDimensions>()).clone();

        // Place the camera
        init_camera(world, &dimensions);

        init_boards_prefab(world, &mut self.progress);
    }

    fn update(&mut self, _data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if self.progress.is_complete() {
            return SimpleTrans::Switch(Box::new(super::InGameState::default()));
        }
        SimpleTrans::None
    }
}

fn init_camera(world: &mut World, dimensions: &ScreenDimensions) {
    // Center the camera in the middle of the screen, and let it cover
    // the entire screen
    let mut transform = Transform::default();
    transform.set_translation_xyz(dimensions.width() * 0.5, dimensions.height() * 0.5, 1.);

    world
        .create_entity()
        .named("camera")
        .with(Camera::standard_2d(dimensions.width(), -dimensions.height()))
        .with(transform)
        .build();
}

fn init_boards_prefab(world: &mut World, progress: &mut ProgressCounter) {
    let handle = world.exec(|loader: PrefabLoader<'_, BoardPrefab>| {
        loader.load("prefabs/boards.ron", RonFormat, progress)
    });

    world.create_entity()
        .with(handle)
        .build();
}
